# Description
This Dockerfile generates an image containing cmake.

# Usage
- `docker build -t arch-git-pip .`
- `docker run -it arch-git-pip`
<br /> OR
- `docker run -it registry.gitlab.com/francoissevestre/dockerfiles/arch-git-pip` 
