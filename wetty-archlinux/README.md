# Description
This Dockerfile generates an image containing wetty on archlinux.

# Usage
- `docker build -t wetty-archlinux .`
- `docker run -it wetty-archlinux <MY_USERNAME> <MY_PASSWORD>`
<br /> OR
- `docker run -it registry.gitlab.com/francoissevestre/dockerfiles/wetty-archlinux <MY_USERNAME> <MY_PASSWORD>` 
