#!/bin/env bash
ssh-keygen -A
/usr/bin/sshd -D &

useradd -m "$1"
echo "$1:$2" | chpasswd
chsh -s /bin/fish "$1"
su "$1" -c 'wetty -p 3000 --ssh-host localhost'
